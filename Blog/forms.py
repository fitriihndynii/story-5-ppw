from django import forms
from .models import ScheduleFitri

class ScheduleFitriForm(forms.ModelForm): 
    class Meta:
        model = ScheduleFitri
        fields = {
            'name',
            'place',
            'category',
            'start_time',
        }
        labels = {
            'name': 'Name of Schedule',
            'start_time': 'Time',
        }
        widgets = {
            'name': forms.TextInput(
                attrs = {
                    'class': 'form-control',
                    'placeholder': 'Type Name of The Schedule',
                }
            ),
            'place': forms.TextInput(
                attrs = {
                    'class': 'form-control',
                    'placeholder': 'Type Place of The Schedule',
                }
            ),
            'category': forms.Select(
                attrs = {
                    'class': 'form-control',
                }
            ),
            'start_time': forms.DateTimeInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'yyyy-mm-dd hour:minute',
                }
            ),
        }