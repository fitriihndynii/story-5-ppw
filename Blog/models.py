from django.db import models

# Create your models here.
class ScheduleFitri(models.Model):
    name = models.CharField(max_length=50)
    place = models.CharField(max_length=50)
    category = models.ForeignKey('Category', on_delete = models.DO_NOTHING)
    start_time = models.DateTimeField()
    def __str__(self):
        return self.name

class Category(models.Model):
    category = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.category