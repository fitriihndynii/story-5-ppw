from django.urls import re_path
from . import views
#url for app
urlpatterns = [
    re_path(r'^$', views.myblog, name='myblog'),
    re_path(r'^about/$', views.about, name='about'),
    re_path(r'^background/$', views.background, name='background'),
    re_path(r'^gallery/$', views.gallery, name='gallery'),
    re_path(r'^schedule/$', views.schedule, name='schedule'),
    re_path(r'^scheduleList/$', views.scheduleList, name='scheduleList'),
    re_path('delete/(?P<id>[0-9]+)/', views.delete, name='delete'),
    re_path(r'^message/$', views.message, name='message'),
    re_path(r'^contact/$', views.contact, name='contact'),
]