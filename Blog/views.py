from django.shortcuts import render, redirect
from .models import ScheduleFitri
from .forms import ScheduleFitriForm

# Create your views here.

def myblog(request):
    return render(request, 'myblog.html')

def about(request):
    return render(request, 'about.html')

def background(request):
    return render(request, 'background.html')

def gallery(request):
    return render(request, 'gallery.html')

def schedule(request):
    schedule = ScheduleFitriForm(request.POST or None)

    if request.method == 'POST':
        if schedule.is_valid():
            schedule.save()
            return redirect('scheduleList')

    argument = {
        'schedule':schedule
    }
    return render(request, 'schedule.html', argument)

def scheduleList(request):
    schedule_list = ScheduleFitri.objects.order_by('start_time')
    argument = {
        'schedule_list': schedule_list,
    }
    return render(request, 'scheduleList.html', argument)

def delete(request,id):
    ScheduleFitri.objects.get(id=id).delete()
    return redirect('scheduleList')

def message(request):
    return render(request, 'message.html')

def contact(request):
    return render(request, 'contact.html')