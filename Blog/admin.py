from django.contrib import admin
from Blog.models import ScheduleFitri, Category

# Register your models here.
admin.site.register(ScheduleFitri)
admin.site.register(Category)
